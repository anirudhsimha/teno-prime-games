package com.tenoapp.interactivegames.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.tenoapp.interactivegames.mathgame.AppType;

@Repository
public interface GameRepository extends JpaRepository<GameEntity, Integer> {

    @Modifying
    @Query(nativeQuery = true, value = "INSERT INTO interactive_game_stats(app_type, max_score, user_id, user_first_name, user_last_name, child_id, child_first_name"
            + ", child_last_name, school_id, school_name, attempt_count, create_date, update_date, role) values(:#{#stats.getAppType().ordinal()}, :#{#stats.getMaxScore()}"
            + ", :#{#stats.getUserId()}, :#{#stats.getUserFirstName()}, :#{#stats.getUserLastName()}, :#{#stats.getChildId()}, :#{#stats.getChildFirstName()}"
            + ", :#{#stats.getChildLastName()}, :#{#stats.getSchoolId()}, :#{#stats.getSchoolName()}"
            + ", :#{#stats.getAttemptCount()}, :#{#stats.getCreateDate()}, :#{#stats.getUpdateDate()}, :#{#stats.getRole().ordinal()})"
            + " ON DUPLICATE KEY UPDATE attempt_count = attempt_count + :#{#stats.getAttemptCount()}, max_score = greatest(max_score, :#{#stats.getMaxScore()})")
    public Integer upsertScore(@Param("stats") GameEntity stats);

    public List<GameEntity> findByChildId(Integer childId);

    public Long countByAppType(AppType type);

    public List<GameEntity> findByUserIdAndAppType(Integer userId, AppType type);

    public Page<GameEntity> findByAppType(AppType type, Pageable page);
}
