package com.tenoapp.interactivegames.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface GamePracticeRepository extends JpaRepository<GamePracticeEntity, Integer> {

    @Modifying
    @Query(nativeQuery = true, value = "INSERT INTO interactive_practice_game_stats(app_type, game_mode, game_level, time_spent, user_id, user_first_name"
            + ", user_last_name, child_id, child_first_name"
            + ", child_last_name, school_id, school_name, attempt_count, create_date, update_date, role) values(:#{#stats.getAppType().ordinal()}"
            + ", :#{#stats.getGameMode().ordinal()}, :#{#stats.getGameLevel().ordinal()}, :#{#stats.getTimeSpent()}"
            + ", :#{#stats.getUserId()}, :#{#stats.getUserFirstName()}, :#{#stats.getUserLastName()}, :#{#stats.getChildId()}, :#{#stats.getChildFirstName()}"
            + ", :#{#stats.getChildLastName()}, :#{#stats.getSchoolId()}, :#{#stats.getSchoolName()}"
            + ", :#{#stats.getAttemptCount()}, :#{#stats.getCreateDate()}, :#{#stats.getUpdateDate()}, :#{#stats.getRole().ordinal()})"
            + " ON DUPLICATE KEY UPDATE attempt_count = attempt_count + :#{#stats.getAttemptCount()}, time_spent = time_spent + :#{#stats.getTimeSpent()}")
    public Integer upsertScore(@Param("stats") GamePracticeEntity stats);

}
