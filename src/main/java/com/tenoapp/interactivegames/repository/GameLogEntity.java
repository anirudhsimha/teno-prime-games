package com.tenoapp.interactivegames.repository;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.tenoapp.interactivegames.mathgame.AppType;
import com.tenoapp.interactivegames.mathgame.GameMode;
import com.tenoapp.interactivegames.mathgame.Role;

import lombok.Data;

@Entity
@Table(name="interactive_game_log")
@Data
public class GameLogEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Column(name = "app_type")
    private AppType appType;

    @Column(name="game_mode")
    private GameMode gameMode;
    
    @Column(name = "score")
    private Long score;

    @Column(name = "user_id")
    private Integer userId;

    @Column(name = "role")
    private Role role;

    @Column(name = "user_first_name")
    private String userFirstName;
    
    @Column(name = "user_last_name")
    private String userLastName;

    @Column(name = "child_id")
    private Integer childId;

    @Column(name = "child_first_name")
    private String childFirstName;
    
    @Column(name = "child_last_name")
    private String childLastName;

    @Column(name = "school_id")
    private Integer schoolId;

    @Column(name = "school_name")
    private String schoolName;

    @Column(name = "create_date")
    private Date createDate;

    @Column(name = "update_date")
    private Date updateDate;

}
