package com.tenoapp.interactivegames.repository;

import org.springframework.data.jpa.repository.JpaRepository;

public interface GamePracticeLogRepository extends JpaRepository<GamePracticeLogEntity, Integer> {
    
}
