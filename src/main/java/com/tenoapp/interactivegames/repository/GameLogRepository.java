package com.tenoapp.interactivegames.repository;

import org.springframework.data.jpa.repository.JpaRepository;

public interface GameLogRepository extends JpaRepository<GameLogEntity, Integer> {

}
