package com.tenoapp.interactivegames.mathgame;

public enum Role {
    STUDENT, PARENT
}
