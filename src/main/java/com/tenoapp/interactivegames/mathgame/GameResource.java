package com.tenoapp.interactivegames.mathgame;


import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.tenoapp.interactivegames.repository.GameEntity;

import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Controller
public class GameResource {

    @Autowired
    GameService gameService;

    @ApiResponses(value = { @ApiResponse(code = 200, response = Void.class, message = "data accepted") })
    @PostMapping("/games/score")
    @ResponseBody
    public ResponseEntity<?> upsertScore(HttpServletRequest req, HttpServletResponse res,
            @RequestHeader(value = "access-key", required = true) String userId,
            @RequestHeader(value = "access-token", required = true) String password,
            @Valid @RequestBody GameScoreDetails scoreDetails) {

        gameService.upsertScore(scoreDetails);
        String source = req.getHeader("x-source");
        System.out.println("source================================="+source);
        
        if(!StringUtils.isEmpty(source) && source.equalsIgnoreCase("android"))
        	return ResponseEntity.ok().build();
        else
        {
        	Map<String, Object> retVal = new HashMap<String, Object>();
        	retVal.put("code" , "success");
        	return ResponseEntity.ok(retVal);
        }

    }

    @ApiResponses(value = { @ApiResponse(code = 200, response = Void.class, message = "data accepted") })
    @PostMapping("/games/practice/score")
    @ResponseBody
    public ResponseEntity<?> upsertPracticeScore(HttpServletRequest req, HttpServletResponse res,
            @RequestHeader(value = "access-key", required = true) String userId,
            @RequestHeader(value = "access-token", required = true) String password,
            @Valid @RequestBody GamePracticeScoreDetails scoreDetails) {

        gameService.upsertPracticeScore(scoreDetails);
        String source = req.getHeader("x-source");
        System.out.println("source================================="+source);
        
        if(!StringUtils.isEmpty(source) && source.equalsIgnoreCase("android"))
        	return ResponseEntity.ok().build();
        else
        {
        	Map<String, Object> retVal = new HashMap<String, Object>();
        	retVal.put("code" , "success");
        	return ResponseEntity.ok(retVal);
        }

    }

    @ApiResponses(value = {
            @ApiResponse(code = 200, response = GameEntity.class, responseContainer = "List", message = "top 50 scores retrived"),
            @ApiResponse(code = 204, response = Void.class, message = "no content") })
    @GetMapping("/games/topscores")
    @ResponseBody
    public ResponseEntity<?> getTopScores(HttpServletRequest req, HttpServletResponse res,
            @RequestHeader(value = "access-key", required = true) String userId,
            @RequestHeader(value = "access-token", required = true) String password,
            @RequestParam(value = "size", required = false, defaultValue = "50") Integer size,
            @RequestParam(value = "type", required = true) AppType type) {

        List<GameEntity> entities = gameService.getTopNScores(size, type);
        log.info(entities.toString());

        if (entities == null || entities.isEmpty()) {
            return ResponseEntity.noContent().build();
        }

        return ResponseEntity.ok(entities);

    }

    @ApiResponses(value = {
            @ApiResponse(code = 200, response = Long.class, message = "unique user count retrived"),
            @ApiResponse(code = 204, response = Void.class, message = "no content") })
    @GetMapping("/games/unique-users")
    @ResponseBody
    public ResponseEntity<?> getUniqueUsers(HttpServletRequest req, HttpServletResponse res,
            @RequestHeader(value = "access-key", required = true) String userId,
            @RequestHeader(value = "access-token", required = true) String password,
            @RequestParam(value = "type", required = true) AppType type) {

        Long userCount = gameService.countByAppType(type);
        return ResponseEntity.ok(userCount);
    }
    
    @ApiResponses(value = {
            @ApiResponse(code = 200, response = GameEntity.class, responseContainer = "List", message = "user profile(s) retrieved"),
            @ApiResponse(code = 204, response = Void.class, message = "no content") })
    @GetMapping("/games/user-profile")
    @ResponseBody
    public ResponseEntity<?> getUserProfile(HttpServletRequest req, HttpServletResponse res,
            @RequestHeader(value = "access-key", required = true) String userId,
            @RequestHeader(value = "access-token", required = true) String password,
            @RequestParam(value = "type", required = true) AppType type) {

        List<GameEntity> userProfiles = gameService.findByUserIdAndAppType(Integer.valueOf(userId), type);
        return ResponseEntity.ok(userProfiles);
    }


}
