package com.tenoapp.interactivegames.mathgame;

import java.math.BigDecimal;

import javax.validation.constraints.NotNull;

import lombok.Data;

@Data
public class GamePracticeScoreDetails {

    @NotNull
    private GamePracticeMode gameMode;
    
    @NotNull
    private GameLevel gameLevel;
    
    @NotNull
    private AppType appType;
    
    @NotNull
    private BigDecimal timeSpent;
    
    @NotNull
    private Integer userId;
    
    private String userFirstName;
    
    private String userLastName;
    
    @NotNull
    private Integer childId;
    
    private String childFirstName;
    
    private String childLastName;
    
    @NotNull
    private Integer schoolId;
    
    private String schoolName;
    
    @NotNull
    private Role role;
}
