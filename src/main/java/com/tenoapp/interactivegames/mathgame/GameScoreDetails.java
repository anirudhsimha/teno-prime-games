package com.tenoapp.interactivegames.mathgame;

import javax.validation.constraints.NotNull;

import lombok.Data;

@Data
public class GameScoreDetails {

    @NotNull
    private GameMode gameMode;
    
    @NotNull
    private AppType appType;
    
    @NotNull
    private Long score;
    
    @NotNull
    private Integer userId;
    
    private String userFirstName;
    
    private String userLastName;
    
    private Integer childId;
    
    private String childFirstName;
    
    private String childLastName;
    
    @NotNull
    private Integer schoolId;
    
    private String schoolName;
    
    @NotNull
    private Role role;
}
