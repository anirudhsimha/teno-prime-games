package com.tenoapp.interactivegames.mathgame;

public enum GameLevel {
    EASY, MEDIUM, HARD;
}
