package com.tenoapp.interactivegames.mathgame;

public enum GamePracticeMode {
    ADD, SUB, MUL, DIV;
}
