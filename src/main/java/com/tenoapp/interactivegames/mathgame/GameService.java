package com.tenoapp.interactivegames.mathgame;

import java.util.Date;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.tenoapp.interactivegames.repository.GameEntity;
import com.tenoapp.interactivegames.repository.GameLogEntity;
import com.tenoapp.interactivegames.repository.GameLogRepository;
import com.tenoapp.interactivegames.repository.GamePracticeEntity;
import com.tenoapp.interactivegames.repository.GamePracticeLogEntity;
import com.tenoapp.interactivegames.repository.GamePracticeLogRepository;
import com.tenoapp.interactivegames.repository.GamePracticeRepository;
import com.tenoapp.interactivegames.repository.GameRepository;

@Service
public class GameService {

    @Autowired
    GameRepository gameRepository;
    
    @Autowired
    GamePracticeRepository practiceGameRepository;
    
    @Autowired
    GameLogRepository gameLogRepository;
    
    @Autowired
    GamePracticeLogRepository gamePracticeLogRepository;

    @Transactional
    public void upsertScore(@Valid GameScoreDetails scoreDetails) {

        //TODO INSERT INTO LOG
       
        GameLogEntity logEntity = new GameLogEntity();
        logEntity.setAppType(scoreDetails.getAppType());
        logEntity.setScore(scoreDetails.getScore());
        logEntity.setCreateDate(new Date());
        logEntity.setChildId(scoreDetails.getChildId());
        logEntity.setChildFirstName(scoreDetails.getChildFirstName());
        logEntity.setChildLastName(scoreDetails.getChildLastName());
        logEntity.setRole(scoreDetails.getRole());
        logEntity.setSchoolId(scoreDetails.getSchoolId());
        logEntity.setSchoolName(scoreDetails.getSchoolName());
        logEntity.setUserId(scoreDetails.getUserId());
        logEntity.setUserFirstName(scoreDetails.getUserFirstName());
        logEntity.setUserLastName(scoreDetails.getUserLastName());
        logEntity.setGameMode(scoreDetails.getGameMode());
        
        if(scoreDetails.getRole().equals(Role.STUDENT)) {
            logEntity.setChildId(0);
            logEntity.setChildFirstName("");
            logEntity.setChildLastName("");
        }
        
        gameLogRepository.save(logEntity);
        
        GameEntity entity = new GameEntity();
        entity.setMaxScore(scoreDetails.getScore());
        entity.setAppType(scoreDetails.getAppType());
        entity.setAttemptCount(1);
        entity.setCreateDate(new Date());
        entity.setChildId(scoreDetails.getChildId());
        entity.setChildFirstName(scoreDetails.getChildFirstName());
        entity.setChildLastName(scoreDetails.getChildLastName());
        
        entity.setRole(scoreDetails.getRole());
        entity.setSchoolId(scoreDetails.getSchoolId());
        entity.setSchoolName(scoreDetails.getSchoolName());
        entity.setUserId(scoreDetails.getUserId());
        entity.setUserFirstName(scoreDetails.getUserFirstName());
        entity.setUserLastName(scoreDetails.getUserLastName());
        
        if(scoreDetails.getRole().equals(Role.STUDENT)) {
            entity.setChildId(0);
            entity.setChildFirstName("");
            entity.setChildLastName("");
        }

        gameRepository.upsertScore(entity);
    }

    public List<GameEntity> getTopNScores(Integer size, AppType type) {
        Pageable page = PageRequest.of(0, size, Direction.DESC, "maxScore");
        return gameRepository.findByAppType(type, page).getContent();
    }

    public List<GameEntity> findByUserIdAndAppType(Integer userId, AppType type) {
        return gameRepository.findByUserIdAndAppType(userId, type);
    }

    @Transactional
    public void upsertPracticeScore(@Valid GamePracticeScoreDetails scoreDetails) {
        
        //TODO INSERT INTO LOG
        
        GamePracticeLogEntity entityLog = new GamePracticeLogEntity();
        
        entityLog.setTimeSpent(scoreDetails.getTimeSpent());
        entityLog.setAppType(scoreDetails.getAppType());
        entityLog.setGameLevel(scoreDetails.getGameLevel());
        entityLog.setGameMode(scoreDetails.getGameMode());
        entityLog.setCreateDate(new Date());
        entityLog.setChildId(scoreDetails.getRole().equals(Role.PARENT) ? scoreDetails.getChildId() : 0);
        entityLog.setChildFirstName(scoreDetails.getChildFirstName());
        entityLog.setChildLastName(scoreDetails.getChildLastName());
        entityLog.setRole(scoreDetails.getRole());
        entityLog.setSchoolId(scoreDetails.getSchoolId());
        entityLog.setSchoolName(scoreDetails.getSchoolName());
        entityLog.setUserId(scoreDetails.getUserId());
        entityLog.setUserFirstName(scoreDetails.getUserFirstName());
        entityLog.setUserLastName(scoreDetails.getUserLastName());
        
        
        if(scoreDetails.getRole().equals(Role.STUDENT)) {
            entityLog.setChildId(0);
            entityLog.setChildFirstName("");
            entityLog.setChildLastName("");
        }
        
        gamePracticeLogRepository.save(entityLog);
        
        GamePracticeEntity entity = new GamePracticeEntity();
        entity.setTimeSpent(scoreDetails.getTimeSpent());
        entity.setAppType(scoreDetails.getAppType());
        entity.setGameMode(scoreDetails.getGameMode());
        entity.setGameLevel(scoreDetails.getGameLevel());
        entity.setAttemptCount(1);
        entity.setCreateDate(new Date());
        entity.setChildId(scoreDetails.getRole().equals(Role.PARENT) ? scoreDetails.getChildId() : 0);
        entity.setChildFirstName(scoreDetails.getChildFirstName());
        entity.setChildLastName(scoreDetails.getChildLastName());
        entity.setRole(scoreDetails.getRole());
        entity.setSchoolId(scoreDetails.getSchoolId());
        entity.setSchoolName(scoreDetails.getSchoolName());
        entity.setUserId(scoreDetails.getUserId());
        entity.setUserFirstName(scoreDetails.getUserFirstName());
        entity.setUserLastName(scoreDetails.getUserLastName());
        
        if(scoreDetails.getRole().equals(Role.STUDENT)) {
            entity.setChildId(0);
            entity.setChildFirstName("");
            entity.setChildLastName("");
        }

        practiceGameRepository.upsertScore(entity);
        
    }
    
    public Long countByAppType(AppType type) {
        return gameRepository.countByAppType(type);
    }

}
