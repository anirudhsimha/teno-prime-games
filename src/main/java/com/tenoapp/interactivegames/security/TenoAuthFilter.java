
package com.tenoapp.interactivegames.security;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;

import lombok.extern.slf4j.Slf4j;

//@WebFilter(filterName = "teno-diary-filter", urlPatterns = { "/*" })
@Slf4j
@Component
public class TenoAuthFilter implements Filter {

    @Value("${teno.auth.url}")
    private String tenoRestURL;

    @Value("${teno.auth.token}")
    private String serviceToken;

    @Autowired
    @Qualifier("tenoRequestData")
    TenoRequestData tenoRequestData;

    @Override
    public void destroy() {
        System.out.println("TenoAuthFilter : destroy");
    }

    /**
     * TODO: try filter response using ResponseEntity
     */
    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
            throws IOException, ServletException {
        HttpServletResponse httpResponse = ((HttpServletResponse) response);
        HttpServletRequest httpRequest = ((HttpServletRequest) request);

        String requestURL = httpRequest.getRequestURL().toString();

        try {
            boolean allowReq = false;

            if (isPublicUrl(requestURL, httpRequest)) {
                allowReq = true;
            } else {
                allowReq = validateTenoUser(request);
            }

            if (allowReq) {
                chain.doFilter(request, response);
            } else {
                httpResponse.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
            }
        } catch (Throwable e) {
            log.error("Fatal Error in authetication:", e);
            try {
                httpResponse.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            } catch (JSONException e1) {
                log.error("Fatal Error in authetication1:", e);
            }

        }

    }

    private Boolean validateTenoUser(ServletRequest servletRequest) {
        HttpServletRequest httpServletRequest = ((HttpServletRequest) servletRequest);
        String userId = (String) httpServletRequest.getHeader("access-key");
        String accessToken = (String) httpServletRequest.getHeader("access-token");
        // String requestUri = httpServletRequest.getRequestURI();

        if (StringUtils.isEmpty(userId) || StringUtils.isEmpty(accessToken)) {
            // if credentials not present, fail !
            return false;
        } else {
            try {
                String command = "{\"accessToken\":\"" + accessToken
                        + "\",\"command\":\"get-user-entity-token\",\"token\":\"" + serviceToken + "\"}";
                log.debug("authRequestCommand {}", command);
                // make teno auth request, in case of request fail, we fail the current auth request
                HttpResponse<TenoUserAuthResponse> tenoResponse = Unirest.get(tenoRestURL)
                        .queryString("command", command).asObject(TenoUserAuthResponse.class);
                if (tenoResponse.getStatus() == 200) {
                    TenoUserAuthResponse authResponse = tenoResponse.getBody();
                    // only if we are able to make successful teno auth call and
                    // if it returns sucess, we assume our current request is successful
                    log.debug("authResponse {} {} {} {}", accessToken, authResponse.getResponseCode(),
                            authResponse.getResponseMessage(), authResponse.getUserObject());
                    if (authResponse.getResponseCode().equalsIgnoreCase("success")) {
                        JSONObject loggedInUser = new JSONObject(authResponse.getUserObject());
                        // access-key should match access-token
                        if (Integer.valueOf(userId) == loggedInUser.optInt("id")) {
                            tenoRequestData.setLoggedInUser(loggedInUser);
                            return true;
                        } else
                            return false;
                    } else {
                        return false;
                    }
                } else {
                    return false;
                }
            } catch (UnirestException exception) {
                log.error("auth_service_failure {} {}", exception.getMessage(), exception);
                return false;
            } catch (Exception exception) {
                log.error("auth_service_failure {} {}", exception.getMessage(), exception);
                return false;
            }
        }
    }

    private static boolean isPublicUrl(String requestUri, HttpServletRequest request) {
        boolean isPublic = false;
        // list all GET public resource
        if ("GET".equalsIgnoreCase(request.getMethod())
                && (requestUri.contains("swagger") || requestUri.contains("api-docs"))) {
            isPublic = true;
        } // list all POST public resource
        else if (requestUri.contains("health-check")) {
            isPublic = true;
        } // anything that doesn't match above is private resource
        else if (requestUri.contains("vendor")) {
            isPublic = true;
        }
        return isPublic;
    }

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        SpringBeanAutowiringSupport.processInjectionBasedOnServletContext(this, filterConfig.getServletContext());
        log.debug("TenoAuthFilter : intitiated : " + tenoRestURL);

    }
}
