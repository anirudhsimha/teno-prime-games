
package com.tenoapp.interactivegames.security;

import java.util.HashMap;
import java.util.Map;

import javax.validation.Valid;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class TenoUserAuthResponse
{
    @JsonProperty("userObject")
    private String              userObject;
    @JsonProperty("responseMessage")
    private String              responseMessage;
    @JsonProperty("responseCode")
    private String              responseCode;
    @JsonIgnore
    @Valid
    private Map<String, Object> additionalProperties    = new HashMap<String, Object>();
}
