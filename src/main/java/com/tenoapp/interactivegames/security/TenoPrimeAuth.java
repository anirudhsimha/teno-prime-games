package com.tenoapp.interactivegames.security;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.auth0.jwt.interfaces.DecodedJWT;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class TenoPrimeAuth {

    @Value("${teno.prime.jwt.secret}")
    private String tenoPrimeJwtSecret;

    public boolean authPrimeRequest(String primeDigest) {
        if (primeDigest == null || primeDigest.isEmpty()) {
            return false;
        }
        // verifier will throw exception if the end date is expired
        Algorithm algorithm;
        try {
            algorithm = Algorithm.HMAC256(tenoPrimeJwtSecret);
            JWTVerifier verifier = JWT.require(algorithm).withIssuer("prime").build(); // Reusable verifier instance
            DecodedJWT jwt = verifier.verify(primeDigest);
            log.info("~ TenoPrimeAuth.authRequest ~ {}", jwt.getExpiresAt().toString());
            return true;
        } catch (JWTVerificationException e) {
            log.error("~ TenoPrimeAuth.authRequest ~ {} JWTVerificationException  {}", primeDigest, e.getMessage());
            return false;
        }
    }

}
