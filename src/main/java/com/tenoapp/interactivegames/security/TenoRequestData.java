package com.tenoapp.interactivegames.security;

import org.json.JSONObject;

import lombok.Data;

@Data
public class TenoRequestData {

    private JSONObject loggedInUser;
    private String serialId;

    // TODO: add logged in user mdoel
}
