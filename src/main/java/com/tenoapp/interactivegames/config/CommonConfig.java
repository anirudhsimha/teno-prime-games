package com.tenoapp.interactivegames.config;

import java.io.IOException;
import java.util.concurrent.Executor;
import java.util.concurrent.ThreadPoolExecutor;

import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.web.filter.CommonsRequestLoggingFilter;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.mashape.unirest.http.ObjectMapper;
import com.mashape.unirest.http.Unirest;
import com.tenoapp.interactivegames.security.TenoRequestData;

import lombok.extern.slf4j.Slf4j;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.Tag;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;

@Configuration
@Slf4j
public class CommonConfig {

    @Bean
    public Docket productApi(final ApplicationContext applicationContext) {
        return new Docket(DocumentationType.SWAGGER_2).select().apis(RequestHandlerSelectors.basePackage("com.tenoapp"))
                .paths(PathSelectors.regex("/.*")).build();
    }

    @Bean("tenoRequestData")
    @Scope(value = "request", proxyMode = ScopedProxyMode.TARGET_CLASS)
    public TenoRequestData tenoRequestData() {
        TenoRequestData tenoRequestData = new TenoRequestData();
        String serial = Math.random() * 100000 + "";
        log.info("~ generating request scoped bean {} ~", serial);
        tenoRequestData.setSerialId(serial);
        return tenoRequestData;
    }
    
    @Bean
    public CommonsRequestLoggingFilter requestLoggingFilter()
    {
        // to make this work
        // SET logging.level.org.springframework.web.filter.CommonsRequestLoggingFilter=DEBUG
        CommonsRequestLoggingFilter crlf = new CommonsRequestLoggingFilter();
        crlf.setIncludeClientInfo(true);
        crlf.setIncludeQueryString(true);
        crlf.setIncludePayload(true);
        crlf.setIncludeHeaders(true);
        return crlf;
    }

    @Bean(name = "gameThreadPoolTaskExecutor")
    public Executor threadPoolTaskExecutor() {

        ThreadPoolTaskExecutor threadpool = new ThreadPoolTaskExecutor();
        threadpool.setCorePoolSize(5);
        threadpool.setMaxPoolSize(10);
        threadpool.setQueueCapacity(1000);
        threadpool.setRejectedExecutionHandler(new ThreadPoolExecutor.CallerRunsPolicy());
        threadpool.setWaitForTasksToCompleteOnShutdown(true);
        threadpool.setThreadNamePrefix("wsdelete-");
        return new ThreadPoolTaskExecutor();
    }

    @EventListener(ApplicationReadyEvent.class)
    public void doSomethingAfterStartup() {
        Unirest.setObjectMapper(new ObjectMapper() {
            private com.fasterxml.jackson.databind.ObjectMapper jacksonObjectMapper = new com.fasterxml.jackson.databind.ObjectMapper();

            public <T> T readValue(String value, Class<T> valueType) {
                try {
                    return jacksonObjectMapper.readValue(value, valueType);
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }

            public String writeValue(Object value) {
                try {
                    return jacksonObjectMapper.writeValueAsString(value);
                } catch (JsonProcessingException e) {
                    throw new RuntimeException(e);
                }
            }
        });
        log.info("hello world, I have just started up");
    }

}
